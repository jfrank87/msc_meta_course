#!/bin/bash
set -o errexit;

# Script: run_analysis,sh
# This script runs the complete metagenome analysis described in the course guide
# Commands may deviate slightly from the manual but the result is exactly the same

# MSc Microbiology: Microbiology Introduction (NWI-BM069)
# Metagenomics lab course
# November 1, 8 2017 - Radboud University Nijmegen

# Estimated running time  using 5 cores: between 30-120 mins depending on system hardware 
# and configuration

# The script assumes all required software and dependencies are installed on your system
# For easy installation and setup of required programs it is recommended to setup a 
# virtual environment. Instructions and files necessary to recreate the correct 
# environment and software can be found after completion of the coure on GitLab: 
# https://gitlab.com/jfrank87/msc_meta_course

# J .Frank - j.frank@science.ru.nl


# Setup
# ========================================================================================

# Maximum number of CPUs to use
cpu=5

# Activate virtual environment (Conda) 
# For instructions on setting up a virtual environment, refer to GitLab
source activate meta_course


# 1. Input sequencing data
# ========================================================================================

# Make a directory "1_data" to store the sequencing data
mkdir 1_data

# Symlink the sequencing data (create "shortcuts") for the 2 samples/4 FASTQ files 
# to 1_data
find ../mock_data/ -name "*gz" -exec ln -s ../{} 1_data \;


# 2. Quality control (pre trimming)- FastQC
# ========================================================================================

mkdir 2_qc_pretrim

# Run FastQC
fastqc -o 2_qc_pretrim/ --threads $cpu 1_data/*.fq.gz


# 3. Read trimming - BBDuk (BBTools)
# ========================================================================================

mkdir 3_trimming

# Perform trimming using bbduk.sh
bbduk.sh in1=1_data/sample_a_R1.fq.gz in2=1_data/sample_a_R2.fq.gz out1=3_trimming/sample_a_trim_R1.fq.gz out2=3_trimming/sample_a_trim_R2.fq.gz qtrim=r trimq=20 maq=20 minlength=120 maxns=-1 threads=$cpu
bbduk.sh in1=1_data/sample_b_R1.fq.gz in2=1_data/sample_b_R2.fq.gz out1=3_trimming/sample_b_trim_R1.fq.gz out2=3_trimming/sample_b_trim_R2.fq.gz qtrim=r trimq=20 maq=20 minlength=120 maxns=-1 threads=$cpu


# 4. Quality control (post trimming) - FastQC
# ========================================================================================

mkdir 4_qc_posttrim

# Use FASTQC once more to create a quality control report, now for the trimmed reads
fastqc -o 4_qc_posttrim/ --threads $cpu 3_trimming/*.fq.gz


# 5. Metagenome co-assembly - MEGAHIT
# ========================================================================================

# Metagenome co-assembly using MEGAHIT
# MEGAHIT creates its own output directory: there is no need to create one yourself
megahit -1 3_trimming/sample_a_trim_R1.fq.gz,3_trimming/sample_b_trim_R1.fq.gz -2 3_trimming/sample_a_trim_R2.fq.gz,3_trimming/sample_b_trim_R2.fq.gz -o 5_co-assembly/ -t $cpu --min-contig-len 1500


# 6. Co-assembly evaluation - QUAST
# ========================================================================================

mkdir 6_assembly_eval

# Run QUAST
quast.py -o 6_assembly_eval/ --no-icarus -t $cpu 5_co-assembly/final.contigs.fa


# 7. Mapping reads to metagenome - BWA, SAMtools
# ========================================================================================

mkdir 7_mapping

# Separately map sample_a and sample_b to the metagenome to get per sample abundance information
# I use a script I wrote to perform the mapping easily (map_pe_bwa.sh)
# The complete mapping procedure is described in Apendix 1 of the course manual

../bin/map_pe_bwa.sh 5_co-assembly/final.contigs.fa 3_trimming/ 7_mapping/ $cpu


# 8. Metagenome binning - MetaBAT2
# ========================================================================================

mkdir -p 8_binning/bins/

# Calculate average coverage for each contig based on the mapping of of reads stored in BAM files
jgi_summarize_bam_contig_depths --outputDepth 8_binning/depth_metabat2.tsv --pairedContigs 8_binning/paired_metabat2.tsv --minContigDepth 2 7_mapping/*.sorted.bam

# Metagenome binning
metabat2 -a 8_binning/depth_metabat2.tsv -i 5_co-assembly/final.contigs.fa -o 8_binning/bins/metabat_bin -t $cpu


# 9. Binning evaluation - CheckM
# ========================================================================================

mkdir 9_binning_eval

# Single-copy marker gene analysis to evaluate bin quality
# Estimate bin completeness and contamination


# Place bins in the reference genome tree
checkm tree -q -t $cpu --pplacer_threads $cpu -x fa 8_binning/bins/ 9_binning_eval/

# Assess phylogenetic markers found in each bin
checkm tree_qa -q -o 2 --tab_table 9_binning_eval/ > 9_binning_eval/phylogenetic_markers.tsv

# Create lineage-specific marker sets for each bin
checkm lineage_set -q 9_binning_eval/ 9_binning_eval/lineage_markers.mf

# Identify marker genes in bins
checkm analyze -q -t $cpu 9_binning_eval/lineage_markers.mf -x fa 8_binning/bins/ 9_binning_eval/

# Assess bin completeness and contamination
checkm qa -q -o 2 -t $cpu --tab_table 9_binning_eval/lineage_markers.mf 9_binning_eval/ > 9_binning_eval/bin_quality.tsv

# Bar plot of bin completeness, contamination, and strain heterogeneity
checkm bin_qa_plot -q --image_type pdf -x fa 9_binning_eval/ 8_binning/bins/ 9_binning_eval/


# 10. Genome annotation - Prokka
# ========================================================================================

# Annotate a bin/genome
# Preferably annotate a bin with high completion/low contamination, that is well assembled
# (few contigs)

mkdir 10_annotation

prokka --outdir 10_annotation/ --prefix bin_2 --locustag bin_2 --compliant --gcode 11 --cpus 6 --force 8_binning/bins/metabat_bin.2.fa
