# Metagenomics lab course
MSc Microbiology: Microbiology Introduction (NWI-BM069)
November 1, 8 2017 - Radboud University Nijmegen

## Introduction
This repository contains all the course materials used during the metagenomics lab
course. It includes the course manual (folder: doc), sequencing data (folder: mock_data),  
the scripts to rerun theanalysis (folder: bin) and all the files generated (folder: example_run).  
During the lab course, students work on the microbiology MicroARB server. Data and results  
are stored in `/data/data/students/meta_course_2017`  

## Running the analysis on your own computer
This section describes how to setup and configure a computer running Linux (Ubuntu) in 
order to rerun the analysis performed during the metagegenomics lab course. The 
instructions below likelyy also works for MacOS, however this has not been tested.

### Virtual environment
A tailor-made virtual environment was created for the course (called "meta_course").
in which most programs (and their dependencies) were installed.This enviroment can easily 
be recreated on another Linux computer, saving you the trouble of installing these programs 
manually yourself.

#### Download and install Anaconda
First you need to dowloand and install Anaconda. 
If asked, choose the python 3  version.
https://www.anaconda.com/downloads

#### Recreate virtual environment
Use the `virtenv_spec-file.txt` file to recreate the meta_course environment:
`conda create --name meta_course --file virtenv_spec-file.txt`

You can now use the new environment
`source activate meta_course`

### Install MetaBAT2 - Metagenome binning
MetaBAT is not available through conda.
The easiest way to obtain MetaBAT2 is to download one of the available binaries here:
https://bitbucket.org/berkeleylab/metabat/downloads/

```bash
# Example
wget https://bitbucket.org/berkeleylab/metabat/downloads/metabat-static-binary-linux-x64_v2.12.1.tar.gz
tar xzvf metabat-static-binary-linux-x64_v2.12.1.tar.gz
```

#### Add MetaBAT2 to $PATH
To make sure the system "finds" your installation of MetaBAT2, you have to do one of 
the  following: 

1 Add MetaBAT2 to your PATH by either
* Addding the following line to your `~/.bash_profile` or `~/.bashrc` and start a new session
`export PATH="/path/to/metabat:$PATH"` 
Modify `/path/to/metabat` to point to your installation of MetaBAT2 
* Run the above line in the terminal every time you start a new session
2  Call MetaBAT2 using absolute paths in stead, e.g.
`/usr/local/bin/metabat/jgi_summarize_bam_contig_depths`
`/usr/local/bin/metabat/metabat2`

### CheckM database
CheckM is installed into the virtual environment.
However, CheckM requires a database to successfully carry out its tasks.
Official documentation can be found here:
https://github.com/Ecogenomics/CheckM/wiki/Installation

To install the required files, run:
`checkm data update`

This will prompt you for an installation directory for the required data files. 
You can update the data files in the future by re-running this command. 
If you are unable to automatically download these files (e.g., you are behind a proxy), 
the files can be manually downloaded from 
https://data.ace.uq.edu.au/public/CheckM_databases/checkm_data_v1.0.7.tar.gz. 

Decompress this file to an appropriate folder and run the following command to inform 
CheckM of where the files have been placed.
`checkm data setRoot <data_directory>` 

### Index Prokka database
Prokka relies on a database for annotation. This database needs to be indexed once before
use. Run the following command:

`prokka --setupdb`
